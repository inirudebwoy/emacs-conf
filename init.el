;;; init.el --- init file
;;; Commentary:
;;; Code:
;; -*- mode: emacs-lisp -*-
(setq default-frame-alist '((font . "Hack-10")))
(setq-default enable-recursive-minibuffers 1)
(setq-default minibuffer-depth-indicate-mode 1)
(setq inhibit-splash-screen t)
(set-face-underline 'font-lock-warning-face "yellow") ; highlight yellow underline
(show-paren-mode 1) ; highlight parentheses
(scroll-bar-mode -1) ; no scroll-bar
(menu-bar-mode -1) ; no pasek menu
(tool-bar-mode -1) ; no toolbar
(fset 'yes-or-no-p 'y-or-n-p)
(setq confirm-kill-emacs 'yes-or-no-p)
(setq column-number-mode t)
(savehist-mode 1) ; save minibuffer history
(global-font-lock-mode 1) ; kolorowanie skladni
(mouse-wheel-mode 1)
(setq transient-mark-mode 1)		; podświetlanie zaznaczenia
(setq scroll-step 1)			; przewijanie po jednej linii
(setq delete-old-versions 1)
(setq-default c-basic-offset 4)
(setq-default c-basic-indent 4)
(setq-default indent-tabs-mode nil)	; use only spaces and no tabs
(setq tab-stop-list '(4 8 12 16 20 24 28 32 36 40 44 48 52 56 60 64 68 72 76 80))
(setq-default default-tab-width 4)
(setq comment-column 79)
(setq browse-url-browser-function 'w3m-browse-url)
(autoload 'w3m-browse-url "w3m" "Ask a WWW browser to show a URL." t)
;; optional keyboard short-cut
(global-set-key "\C-xm" 'browse-url-at-point)
(global-set-key (kbd "C-z") ctl-x-map)
(push "~/.emacs.d/bin" exec-path)
;; XAML
(add-to-list 'auto-mode-alist '("\\.xaml\\'" . xml-mode))

;; VC use only needed
(setq vc-handled-backends (quote (Hg Git)))

;; PACKAGE repos
(setq package-archives '(("gnu" . "http://elpa.gnu.org/packages/")
                         ("marmalade" . "http://marmalade-repo.org/packages/")
                         ("melpa" . "http://melpa.milkbox.net/packages/")))
(package-initialize)

(defun mk-install-rad-packages ()
  "Install only the sweetest of packages."
  (interactive)
  (package-refresh-contents)
  (mapc #'(lambda (package)
            (unless (package-installed-p package)
              (package-install package)))
        '(
          autopair
          cl-lib
          concurrent
          ctable
          ctags
          dash
          deferred
          epc
          epl
          f
          flycheck
          framemove
          git-commit-mode
          git-rebase-mode
          helm
          helm-projectile
          highlight-symbol
          ido-ubiquitous
          ido-vertical-mode
          jedi
          lua-mode
          magit
          monky
          org
          pkg-info
          popup
          projectile
          python-environment
          s
          slime
          solarized-theme
          undo-tree
          yasnippet
          zop-to-char)))

(setq magit-last-seen-setup-instructions "1.4.0")

(require 'helm-config)
(setq helm-always-two-windows t)
(global-set-key (kbd "M-x") 'helm-M-x)
(global-set-key (kbd "M-y") 'helm-show-kill-ring)
(global-set-key (kbd "C-x b") 'helm-mini)
(global-set-key (kbd "C-x C-f") 'helm-find-files)
(global-set-key (kbd "C-c h s") 'helm-semantic-or-imenu)
(global-set-key (kbd "C-c h m") 'helm-man-woman)
(global-set-key (kbd "C-c h f") 'helm-find)
(global-set-key (kbd "C-c h l") 'helm-locate)
(global-set-key (kbd "C-c h o") 'helm-occur)
(global-set-key (kbd "C-c h r") 'helm-resume)
(define-key 'help-command (kbd "C-f") 'helm-apropos)
(define-key 'help-command (kbd "r") 'helm-info-emacs)
(helm-mode 1)
(define-key helm-map (kbd "<tab>") 'helm-execute-persistent-action) ; rebind tab to do persistent action
(define-key helm-map (kbd "C-i") 'helm-execute-persistent-action) ; make TAB works in terminal
(define-key helm-map (kbd "C-z")  'helm-select-action) ; list actions using C-z

;; FLYCHECK
(add-hook 'after-init-hook #'global-flycheck-mode)
(setq flycheck-flake8-maximum-complexity 10)
;; YASNIPPET
(yas-global-mode 1)

(require 'saveplace)
(setq save-place-file "~/.emacs.d/saved-places")
(setq-default save-place t)

;; PROJECTILE
(projectile-global-mode)
(helm-projectile-on)

(define-coding-system-alias 'UTF-8 'utf-8)

;; ABBREV
(setq abbrev-file-name
      "~/.emacs.d/abbrev_defs")
(setq save-abbrevs t)
(dolist (hook '(python-mode-hook
                emacs-lisp-mode-hook))
  (add-hook hook (lambda () (abbrev-mode 1))))

(cua-mode t)
(cua-selection-mode t)

;; ELPY
(elpy-enable)

;; ELPY-FLYCHECK
(when (require 'flycheck nil t)
  (setq elpy-modules (delq 'elpy-module-flymake elpy-modules))
  (add-hook 'elpy-mode-hook 'flycheck-mode))

(require 'framemove)
(windmove-default-keybindings)
(setq framemove-hook-into-windmove t)
(global-set-key (kbd "C-c h")  'windmove-left)
(global-set-key (kbd "C-c t") 'windmove-right)
(global-set-key (kbd "C-c c")    'windmove-up)
(global-set-key (kbd "C-c m")  'windmove-down)

(require 'recentf)
(setq recentf-max-saved-items 200
      recentf-max-menu-items 15)
(recentf-mode 1)
(global-set-key "\C-x\ \C-r" 'recentf-open-files)

;; LINUM
(autoload 'linum-mode "linum" "toggle line numbers on/off" t)
(global-set-key (kbd "C-<f5>") 'linum-mode)

;; Docs
(global-set-key [f1] 'manual-entry)
(setq Man-notify-method 'newframe)

;; By default monky spawns a seperate hg process for every command.
;; This will be slow if the repo contains lot of changes.
;; if `monky-process-type' is set to cmdserver then monky will spawn a single
;; cmdserver and communicate over pipe.
;; Available only on mercurial versions 1.9 or higher

(setq monky-process-type 'cmdserver)

;; AUTOPAIR
(autopair-global-mode) ;; to enable in all buffers
(add-hook 'python-mode-hook
           #'(lambda ()
               (setq autopair-handle-action-fns
                     (list #'autopair-default-handle-action
                           #'autopair-python-triple-quote-action))))

;; highlight TODO, FIXME, BUG
(add-hook 'python-mode-hook
		  (lambda ()
			(font-lock-add-keywords nil
			'(("\\<\\(FIXME\\|TODO\\|BUG\\)" 1 font-lock-warning-face t)))))

(require 'uniquify)
(setq uniquify-buffer-name-style 'reverse)
(setq uniquify-separator "|")
(setq uniquify-after-kill-buffer-p t)
(setq uniquify-ignore-buffers-re "^\\*")

(require 'org-install)
(add-to-list 'auto-mode-alist '("\\.org$" . org-mode))
(define-key global-map "\C-cl" 'org-store-link)
(define-key global-map "\C-ca" 'org-agenda)
(setq org-log-done t)

(setq org-src-fontify-natively t)

;; http://lists.gnu.org/archive/html/help-gnu-emacs/2003-10/msg00577.html
(defadvice pdb (before gud-query-cmdline activate)
  "Provide a better default command line when called interactively."
  (interactive
   (list (gud-query-cmdline pdb-path
			    (file-name-nondirectory buffer-file-name)))))

(require 'zop-to-char)
;;To replace `zap-to-char':
(global-set-key (kbd "M-z") 'zop-to-char)

;; LUA-MODE
(autoload 'lua-mode "lua-mode" "Lua editing mode." t)
(add-to-list 'auto-mode-alist '("\\.lua$" . lua-mode))
(add-to-list 'interpreter-mode-alist '("lua" . lua-mode))

(require 'highlight-symbol)
(global-set-key [(control f3)] 'highlight-symbol-at-point)
(global-set-key [f3] 'highlight-symbol-next)
(global-set-key [(shift f3)] 'highlight-symbol-prev)
(global-set-key [(meta f3)] 'highlight-symbol-prev)

;; HIDESHOW
(defun toggle-hiding (column)
  (interactive "P")
  (if hs-minor-mode
      (if (condition-case nil
              (hs-toggle-hiding)
            (error t))
          (hs-show-all))
    (toggle-selective-display column)))

 (defun toggle-selective-display (column)
   (interactive "P")
   (set-selective-display
    (or column
        (unless selective-display
          (1+ (current-column))))))

(global-set-key (kbd "C-+") 'toggle-hiding)
(global-set-key (kbd "C-\\") 'toggle-selective-display)
(add-hook 'python-mode-hook 'hs-minor-mode)

;; HOOKS
; remove trailing whitespace
(add-hook 'before-save-hook 'delete-trailing-whitespace)
;;(add-hook 'haskell-mode-hook 'turn-on-haskell-indentation)
(add-hook 'haskell-mode-hook 'turn-on-haskell-indent)
;;(add-hook 'haskell-mode-hook 'turn-on-haskell-simple-indent)

;; ediff config
(setq ediff-split-window-function 'split-window-horizontally)

(custom-set-variables
 ;; custom-set-variables was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(initial-frame-alist (quote ((fullscreen . maximized))))
 '(column-number-mode t)
 '(cua-mode t nil (cua-base))
 '(custom-safe-themes (quote ("e16a771a13a202ee6e276d06098bc77f008b73bbac4d526f160faa2d76c1dd0e" "d677ef584c6dfc0697901a44b885cc18e206f05114c8a3b7fde674fce6180879" "fc5fcb6f1f1c1bc01305694c59a1a861b008c534cae8d0e48e4d5e81ad718bc6" "8aebf25556399b58091e533e455dd50a6a9cba958cc4ebb0aab175863c25b9a4" default)))
 '(ecb-options-version "2.40")
 '(helm-completing-read-handlers-alist (quote ((describe-function . helm-completing-read-symbols) (describe-variable . helm-completing-read-symbols) (debug-on-entry . helm-completing-read-symbols) (find-function . helm-completing-read-symbols) (find-tag . helm-completing-read-with-cands-in-buffer) (ffap-alternate-file) (tmm-menubar)(kill-buffer . ido))))
 '(magit-use-overlays nil)
 '(menu-bar-mode nil)
 '(safe-local-variable-values (quote ((buffer-file-coding-system . utf-8-dos) (tweak-tabs . t) (defun untabify-buffer nil "Untabify current buffer" (interactive) (untabify (point-min) (point-max))))))
 '(show-paren-mode t)
 '(tool-bar-mode nil))
(put 'upcase-region 'disabled nil)
(put 'downcase-region 'disabled nil)
(custom-set-faces
 ;; custom-set-faces was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 )

(load-theme 'solarized-dark t)
;;; init.el ends here
